<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//
// Auth::routes();
//
// Route::group(['middleware' => ['guest']], function () {
//
//
// });



// Auth::routes();
Route::get('/', function () {
    return view('formlogin');
});
Route::get('/formlogin','AuthController@formlogin')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/formregister','AuthController@formregister');
Route::get('/logout','AuthController@logout');



Route::group(['middleware' => ['auth','CheckLevel:Admin']],function(){

  Route::get('/home', 'HomeController@index')->name('home');
  //aset
  // Route::get('/assets','AssetsController@index');
  // Route::get('/assets/create','AssetsController@create');
  // Route::get('/assets/{asset}','AssetsController@show');
  // Route::post('/assets','AssetsController@store');
  // Route::delete('/assets/{asset}','AssetsController@destroy');
  // Route::get('/assets/{asset}/edit','AssetsController@edit');
  // Route::patch('/assets/{asset}','AssetsController@update');
  Route::get('/assets/trash','AssetsController@trash');
  Route::get('/assets/restore/{id}','AssetsController@restore');
  Route::get('/assets/deleted_permanent/{id}','AssetsController@deleted_permanent');
  Route::get('/assets/restore_all','AssetsController@restore_all');
  Route::get('/assets/deleted_all','AssetsController@deleted_all');
  Route::get('/assets/{asset}/transaksi','AssetsController@transaksi');
  Route::post('/transaksi','AssetsController@input_transaksi');
  Route::get('/assets_transactions/{asset}/create','AssetTransactionController@create');
  Route::Resource('assets', 'AssetsController');
  //Route::Resource('asset_transactions', 'AssetTransactionController');
  //karyawan
  Route::get('/karyawan','EmployeesController@index');
  Route::get('/karyawanresign','ResignController@index');

  //Cuti Admin
   //Caftar Pengajuan Cuti
   Route::get('/category/trash','CutiController@trash');
  Route::get('/category/restore/{id}','CutiController@restore');
  Route::get('/category/deleted_permanent/{id}','CutiController@deleted_permanent');
  Route::get('/category/restore_all','CutiController@restore_all');
  Route::get('/category/deleted_all','CutiController@deleted_all');
  Route::Resource('category','CutiController');
    //Kategori Cuti
  Route::get('/cuti/trash','CutiController@trash');
  Route::get('/cuti/restore/{id}','CutiController@restore');
  Route::get('/cuti/deleted_permanent/{id}','CutiController@deleted_permanent');
  Route::get('/cuti/restore_all','CutiController@restore_all');
  Route::get('/cuti/deleted_all','CutiController@deleted_all');
  Route::Resource('cuti','CutiController');
 

  //gaji

  //kelola manajemen
    //jabatan
  Route::get('/jabatan','jabatanController@index');
    //corporate_group
    Route::get('/corporate/trash','CorporateController@trash');
    Route::get('/corporate/restore/{id}','CorporateController@restore');
    Route::get('/corporate/deleted_permanent/{id}','CorporateController@deleted_permanent');
    Route::get('/corporate/restore_all','CorporateController@restore_all');
    Route::get('/corporate/deleted_all','CorporateController@deleted_all');
    Route::Resource('corporate','CorporateController');
  //positions
    Route::get('/divisi/trash','DivisionController@trash');
    Route::get('/divisi/restore/{id}','DivisionController@restore');
    Route::get('/divisi/deleted_permanent/{id}','DivisionController@deleted_permanent');
    Route::get('/divisi/restore_all','DivisionController@restore_all');
    Route::get('/divisi/deleted_all','DivisionController@deleted_all');
    Route::Resource('/divisi','DivisionController');
    //grade
    Route::get('/grade/trash','GradeController@trash');
    Route::get('/grade/restore/{id}','GradeController@restore');
    Route::get('/grade/deleted_permanent/{id}','GradeController@deleted_permanent');
    Route::get('/grade/restore_all','GradeController@restore_all');
    Route::get('/grade/deleted_all','GradeController@deleted_all');
  Route::Resource('grade','GradeController');

});

Route::group(['middleware' => ['auth','CheckLevel:Admin,Pegawai']],function(){
  Route::get('/admin','AdminController@dashboard');
});
