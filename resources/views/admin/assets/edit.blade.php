@extends('layouts.master')

@section('content')
<!-- Basic Form Start -->
<div class="basic-form-area mg-b-15">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset mg-t-30">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd">
                            <h1>Edit Data Aset</h1>
                            <div class="sparkline12-outline-icon">
                                <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">

                                        <form action="/assets/{{$asset->id}}" method="post">
                                          @method('patch')
                                          @csrf
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kategori</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" value="{{$asset -> kategori}}" class="form-control" name="kategori" />
                                                    </div>
                                                </div>
                                              </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kode Aset</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" disabled class="form-control" name="kode_aset" value="{{$asset -> kode_aset}}" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Nama Aset</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" name="nama_aset" value="{{$asset -> nama_aset}}" class="form-control" />
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="form-group-inner">
                                                  <div class="row">
                                                      <div class="col-lg-3">
                                                          <label class="login2 pull-right pull-right-pro">Tanggal Beli</label>
                                                      </div>
                                                      <div class="col-lg-9">
                                                          <input type="text" value="{{$asset -> tanggal_beli}}" name="tanggal_beli" class="form-control" />
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="login2 pull-right pull-right-pro">Foto Aset</label>
                                                        </div>
                                                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="file-upload-inner file-upload-inner-right ts-forms">
                                                                <div class="input append-small-btn">
                                                                    <div class="file-button">
                                                                        Browse
                                                                        <input type="file"  onchange="document.getElementById('append-small-btn').value = this.value;">
                                                                    </div>
                                                                    <input type="text" name="gambar" id="append-small-btn" value="{{$asset -> gambar}}" name="gambar" placeholder="No Image selected">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-9">
                                                            <div class="login-horizental cancel-wp pull-right">
                                                                <button class="btn btn-custon-rounded-four btn-warning" type="submit">Cancel</button>
                                                                <button class="btn btn-custon-rounded-four btn-success" type="submit">Save Change</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Form End-->


            @endsection
