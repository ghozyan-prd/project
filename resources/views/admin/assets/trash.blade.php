@extends('layouts.master')

@section('content')
          <!-- Breadcome start-->
          <div class="breadcome-area mg-b-30 des-none">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                          <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="breadcome-heading">
                                      <form role="search" class="">
                  <input type="text" placeholder="Search..." class="form-control">
                  <a href=""><i class="fa fa-search"></i></a>
                </form>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <ul class="breadcome-menu">
                                      <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                      </li>
                                      <li><span class="bread-blod">Dashboard</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          </div>
          <!-- Breadcome End-->
          <!-- Static Table Start -->
          <div class="data-table-area mg-b-15">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="sparkline13-list shadow-reset">
                              <div class="sparkline13-hd">
                                  <div class="main-sparkline13-hd">
                                      <h1>Table <span class="table-project-n">Data</span> Deleted</h1>
                                      <a href="restore_all" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-default"><i class="fa fa-undo"></i>Restore All</a>
                                      <a href="deleted_all" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-danger"><i class="fa fa-trash"></i>Deleted All</a>
                                      <div class="view-mail-action view-mail-ov-d-n">
                                                    <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                                    <span><i class="fa fa-wrench"></i></span>
                                                    <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                                      </div>
                                      <div class="sparkline13-outline-icon">

                                      </div>
                                  </div>
                              </div>
                              <div class="sparkline13-graph">
                                  <div class="dt">
                                    <div class="col-lg-6">
                                          @if (session('restore'))
                                                <div class="alert alert-warning">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <strong>Success!</strong> Data Aset Berhasil Direstore.
                                                </div>
                                            @endif
                                          @if (session('delete'))
                                              <div class="alert alert-danger alert-mg-b">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                                  <strong>Success!</strong> Data Aset Berhasil Dihapus Permanent.
                                              </div>
                                            @endif
                                          @if (session('restore_all'))
                                                  <div class="alert alert-warning">
                                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      <strong>Success!</strong> Data Aset Berhasil Restore Semua.
                                                  </div>
                                              @endif
                                            @if (session('deleted_all'))
                                                    <div class="alert alert-danger alert-mg-b">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <strong>Success!</strong> Data Aset Berhasil Hapus Semua.
                                                    </div>
                                                @endif
                                    </div>
                                      <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                          <thead>
                                              <tr>
                                                  <th data-field="deskripsi">Kode Aset</th>
                                                  <th data-field="email">Nama Aset</th>
                                                  <th data-field="phone">Tanggal Entry</th>
                                                  <th data-field="company">Tanggal Delete</th>
                                                  <th data-field="action">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>

                                            @foreach ($asset  as $ast)
                                              <tr>
                                                  <td>{{$ast -> kode_aset}}</td>
                                                  <td>{{$ast -> nama_aset}}</td>
                                                  <td>{{$ast -> created_at}}</td>
                                                  <td>{{$ast -> deleted_at}}</td>
                                                  <td>

                                                      <a href="/assets/restore/{{$ast->id}}" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-success">Restore</a>
                                                      <a href="/assets/deleted_permanent/{{$ast->id}}" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-danger">Delete Permanent</a>


                                                  </td>
                                              </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <!-- Button start-->
<!-- Button End-->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
<!-- Static Table End -->

            @endsection
