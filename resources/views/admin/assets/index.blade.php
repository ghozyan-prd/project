@extends('layouts.master')

@section('content')
          <!-- Breadcome start-->
          <div class="breadcome-area mg-b-30 des-none">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                          <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="breadcome-heading">
                                      <form role="search" class="">
                  <input type="text" placeholder="Search..." class="form-control">
                  <a href=""><i class="fa fa-search"></i></a>
                </form>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <ul class="breadcome-menu">
                                      <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                      </li>
                                      <li><span class="bread-blod">Dashboard</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          </div>
          <!-- Breadcome End-->
          <!-- Static Table Start -->
          <div class="data-table-area mg-b-15">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="sparkline13-list shadow-reset">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                                    <a href="assets/create" class="btn btn-custon-four btn-default"><i class="fa fa-reply"></i> Tambah Data</a>
                                    <a href="assets/trash" class="btn btn-custon-four btn-warning"><i class="fa fa-undo"></i> Data Trash</a>
                                    <div class="sparkline13-outline-icon">
                                        <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>

                              <div class="sparkline13-graph">
                                  <div class="dt">
                                      <div class="col-lg-6">
                                            @if (session('status'))
                                              <div class="alert alert-success">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                                  <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
                                              </div>
                                              @endif
                                            @if (session('edit'))
                                                  <div class="alert alert-warning">
                                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      <strong>Success!</strong> Data Aset Berhasil Diubah.
                                                  </div>
                                              @endif
                                            @if (session('delete'))
                                                <div class="alert alert-danger alert-mg-b">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <strong>Success!</strong> Data Aset Berhasil Dihapus.
                                                </div>
                                              @endif
                                      </div>

                                      <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                          <thead>
                                              <tr>
                                                  <th data-field="id">No Aset</th>
                                                  <th data-field="name">Kategori</th>
                                                  <th data-field="deskripsi">Kode Aset</th>
                                                  <th data-field="email">Nama Aset</th>
                                                  <th data-field="phone">Tanggal Beli</th>
                                                  <th data-field="company">Gambar</th>
                                                  <th data-field="action">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>

                                            @foreach ($assets as $ast)
                                              <tr>
                                                  <td>{{$loop -> iteration}}</td>
                                                  <td>{{$ast -> kategori}}</td>
                                                  <td>{{$ast -> kode_aset}}</td>
                                                  <td>{{$ast -> nama_aset}}</td>
                                                  <td>{{$ast -> tanggal_beli}}</td>
                                                  <td>{{$ast -> gambar}}</td>
                                                  <td>
                                                    <form action="/assets/{{$ast -> id}}" method="post" class="d-inline">
                                                      @method('delete')
                                                      @csrf
                                                      @foreach($transaksi as $data)
                                                                
                                                          @if($data->tanggal_kembali == '0' and $data->tanggal_pinjam == '0')
                                                                <a href="/assets/{{$ast->id}}/transaksi" class="btn btn-custon-four btn-default">Pengembalian</a>
                                                          @endif

                                                          <!-- @if($data->nama_aset == 'spansaacishd')
                                                          <a href="/assets/{{$ast->id}}/transaksi" class="btn btn-custon-four btn-default">Pengembalian</a>
                                                          @endif -->
                                                      @endforeach
                                                      <a href="/assets/{{$ast->id}}" class="btn btn-custon-four btn-success">Detil</a>
                                                      <a href="/assets/{{$ast->id}}/edit" class="btn btn-custon-four btn-warning">Edit</a>
                                                      <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Are you sure?')" >Delete</button>
                                                    </form>


                                                  </td>
                                              </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <!-- Button start-->
<!-- Button End-->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
<!-- Static Table End -->

            @endsection
