@extends('layouts.master')

@section('content')
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline10-list shadow-reset mg-t-30">
                                <div class="sparkline10-hd">
                                    <div class="main-sparkline10-hd">
                                        <h1>Data Aset</h1>

                                        <a href="{{ url('/aset/create') }}" class="btn btn-primary my-3">Tambah Data</a>
                                            @if (session ('status'))
                                                <div class="alert alert-success">
                                                {{session ('status')}}
                                                </div>
                                            @endif
                                        
                                    </div>
                                </div>
                                <div class="sparkline10-graph">
                                    <div class="static-table-list">
                                        <table class="table border-table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Karyawan</th>
                                                    <th>Jabatan</th>
                                                    <th>Status</th>
                                                    <th>aksi</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($employees as $emp)
                                                <tr>
                                                    <td>{{$loop -> iteration}}</td>
                                                    <td>{{$emp -> nama}}</td>
                                                    <td>{{$emp -> jabatan}}</td>
                                                    <td>{{$emp -> status}}</td>
                                                    <td>
                                                    <a href="/employee/{{$emp ->id}}" class="btn btn-custon-rounded-four btn-info">detail</a>
                                                    <a href="/employee/{{$emp ->id}}/edit" class="btn btn-custon-rounded-four btn-warning">Edit</a>
                                                    </td>
                                                    
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
            @endsection