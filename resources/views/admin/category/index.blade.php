@extends('layouts.master')

@section('content')
          <!-- Breadcome start-->
          <div class="breadcome-area mg-b-30 des-none">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                          <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <div class="breadcome-heading">
                                      <form role="search" class="">
                  <input type="text" placeholder="Search..." class="form-control">
                  <a href=""><i class="fa fa-search"></i></a>
                </form>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <ul class="breadcome-menu">
                                      <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                      </li>
                                      <li><span class="bread-blod">Dashboard</span>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          </div>
          <!-- Breadcome End-->
          <!-- Static Table Start -->
          <div class="data-table-area mg-b-15">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="sparkline13-list shadow-reset">
                            <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Data Corporate Group <span class="table-project-n">Data</span> Table</h1>
                                    <a href="category/create" class="btn btn-custon-four btn-primary"><i class="fa fa-reply"></i> Tambah Data</a>
                                    <a href="category/trash" class="btn btn-custon-four btn-success"><i class="fa fa-undo"></i> Data Trash</a>
                                    <div class="sparkline13-outline-icon">
                                        <span class="sparkline13-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline13-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>

                              <div class="sparkline13-graph">
                                  <div class="dt">
                                      <div class="col-lg-6">
                                            @if (session('status'))
                                              <div class="alert alert-success">
                                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                  </button>
                                                  <strong>Success!</strong> Data Corporate Group Berhasil Ditambahkan.
                                              </div>
                                              @endif
                                            @if (session('edit'))
                                                  <div class="alert alert-warning">
                                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                      <strong>Success!</strong> Data Aset Berhasil Diubah.
                                                  </div>
                                              @endif
                                            @if (session('delete'))
                                                <div class="alert alert-danger alert-mg-b">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <strong>Success!</strong> Data Aset Berhasil Dihapus.
                                                </div>
                                              @endif
                                      </div>

                                      <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                          <thead>
                                              <tr>
                                                  <th data-field="id">No </th>
                                                  <th data-field="kode">Kode Kategori</th>
                                                  <th data-field="nama">jenis cuti</th>
                                                  <th data-field="ket">Keterangan</th>
                                                  <th data-field="nilai">Value</th>
                                                  
                                                  <th data-field="action">Action</th>
                                              </tr>
                                          </thead>
                                          <tbody>

                                            @foreach ($categorys as $categ)
                                              <tr>
                                                  <td>{{$loop -> iteration}}</td>
                                                  <td>{{$categ -> kode_kategori_cuti}}</td>
                                                  <td>{{$categ -> jenis_cuti}}</td>
                                                  <td>{{$categ -> keterangan}}</td>
                                                  <td>{{$categ -> value}}</td>
                                                  <td>
                                                    <form action="/category/{{$categ -> id}}" method="post" class="d-inline">
                                                      @method('delete')
                                                      @csrf
                                                      
                                                      <a href="/category/{{$categ->id}}/edit" class="btn btn-custon-four btn-warning">Edit</a>
                                                      <button type="submit" class="btn btn-custon-four btn-danger" >Delete</button>
                                                    </form>


                                                  </td>
                                              </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                              <!-- Button start-->
<!-- Button End-->
                          </div>
                      </div>
                  </div>
              </div>
          </div>
<!-- Static Table End -->

            @endsection
