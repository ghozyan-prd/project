@extends('layouts.master')

@section('content')
<!-- Basic Form Start -->
<div class="basic-form-area mg-b-15">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset mg-t-30">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd">
                            <h1>Tambah Data Corporate Group</h1>
                            <div class="sparkline12-outline-icon">
                                <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">

                                        <form action="/category" method="post">
                                          @csrf
                                            
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kode Kategori Cuti</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" readonly class="form-control"  name="kode_kategori_cuti" placeholder="Automatic Generate" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Jenis_cuti</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" name="jenis_cuti" class="form-control" />
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Keterangan</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text"  class="form-control"  name="keterangan" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Jenis_cuti</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" name="value" class="form-control" />
                                                    </div>
                                                </div>
                                              </div>
                                               

                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-9">
                                                            <div class="login-horizental cancel-wp pull-right">
                                                                <a href='/category' class="btn btn-custon-four btn-primary ">Cancel</a>
                                                                <button class="btn btn-custon-rounded-four btn-success" type="submit">Save Change</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Form End-->


            @endsection
