@extends('layouts.master')

@section('content')
<!-- Basic Form Start -->
<div class="basic-form-area mg-b-15">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset mg-t-30">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd">
                            <h1>Edit Data Aset</h1>
                            <div class="sparkline12-outline-icon">
                                <span class="sparkline12-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                <span><i class="fa fa-wrench"></i></span>
                                <span class="sparkline12-collapse-close"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">

                                        <form action="/corporate/{{$corporate->id}}" method="post">
                                          @method('patch')
                                          @csrf
                                            
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Kode Corporate</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text"  readonly class="form-control" name="kode_corporate_group" value="{{$corporate -> kode_corporate_group}}" placeholder="Disabled input here..." />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group-inner">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label class="login2 pull-right pull-right-pro">Nama Aset</label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <input type="text" name="nama_corporate_group" value="{{$corporate -> nama_corporate_group}}" class="form-control" />
                                                    </div>
                                                </div>
                                              </div>

                                              
                                               

                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-9">
                                                            <div class="login-horizental cancel-wp pull-right">
                                                                <a href='/corporate'  class="btn btn-custon-four btn-primary ">Cancel</a>
                                                                <button class="btn btn-custon-rounded-four btn-success" type="submit">Save Change</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Basic Form End-->


            @endsection
