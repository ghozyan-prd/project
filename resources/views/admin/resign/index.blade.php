@extends('layouts.master')

@section('content')
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline10-list shadow-reset mg-t-30">
                                <div class="sparkline10-hd">
                                    <div class="main-sparkline10-hd">
                                        <h1>Data Karyawan Resign</h1>

                                       
                                        
                                    </div>
                                </div>
                                <div class="sparkline10-graph">
                                    <div class="static-table-list">
                                        <table class="table border-table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Karyawan</th>
                                                    <th>Mulai Bekerja</th>
                                                    <th>Berhenti Bekerja</th>
                                                    <th>Telp</th>
                                                    <th>aksi</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($employees as $emp)
                                                <tr>
                                                    <td>{{$loop -> iteration}}</td>
                                                    <td>{{$emp -> nama}}</td>
                                                    <td>{{$emp -> start_addendum1}}</td>
                                                    <td>{{$emp -> end_addendum1}}</td>
                                                    <td>{{$emp -> no_hp}}</td>
                                                    <td>
                                                    <a href="/employee/{{$emp ->id}}" class="btn btn-custon-rounded-four btn-info">detail</a>
                                                    <a href="/employee/{{$emp ->id}}/edit" class="btn btn-custon-rounded-four btn-warning">Edit</a>
                                                    </td>
                                                    
                                                </tr>
                                            @endforeach   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
            @endsection