<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Accordion | Adminpro - Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
         ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="/material/img/favicon.ico">
    <!-- Google Fonts
         ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/font-awesome.min.css">
    <!-- adminpro icon CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="/material/css/data-table/bootstrap-editable.css">
    <!-- modals CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/modals.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/normalize.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/alerts.css">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/form/all-type-forms.css">
    <!-- charts CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/c3.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="/material/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="/material/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="/material/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="materialdesign">
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
    <!-- Header top area start-->
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            <nav id="sidebar">
                <div class="sidebar-header">
                    <a href="#"><img src="/img/oezy.jpg" alt="" width="150px" height="150px" />
                    </a>
                    <h3>{{auth()->user()->nama}}</h3>
                    <p>{{auth()->user()->level}}</p>
                    <p>{{ date('l, d-m-Y') }}</p>
                </div>
                <div class="left-custom-menu-adp-wrap">
                    <ul class="nav navbar-nav left-sidebar-menu-pro">

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin') }}"  ><i class="fa big-icon fa-home"></i> Home </a>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Pegawai')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/data') }}"  ><i class="fa big-icon fa-user"></i> Data Diri </a>
                        </li>
                        @endif


                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/assets') }}"  ><i class="fa big-icon fa-desktop"></i> Aset </a>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Pegawai')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/asetp') }}"  ><i class="fa big-icon fa-desktop"></i> Aset </a>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-users"></i> <span class="mini-dn">Karyawan</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="{{ url('/karyawan') }}" class="dropdown-item">Data Karyawan</a>
                                <a href="{{ url('/karyawanresign') }}" class="dropdown-item">Karyawan Resign</a>
                            </div>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-money"></i> <span class="mini-dn">Gaji</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="google-map.html" class="dropdown-item">Google Map</a>
                                <a href="data-maps.html" class="dropdown-item">Data Maps</a>
                                <a href="pdf-viewer.html" class="dropdown-item">Pdf Viewer</a>
                                <a href="x-editable.html" class="dropdown-item">X-Editable</a>
                                <a href="code-editor.html" class="dropdown-item">Code Editor</a>
                                <a href="tree-view.html" class="dropdown-item">Tree View</a>
                                <a href="preloader.html" class="dropdown-item">Preloader</a>
                                <a href="images-cropper.html" class="dropdown-item">Images Cropper</a>
                            </div>
                        </li>
                        @endif
                        @if(auth()->user()->level=='Pegawai')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/gajip') }}"  ><i class="fa big-icon fa-money"></i> Gaji </a>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-user-times"></i> <span class="mini-dn">Cuti</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="{{ url('/daftarcuti') }}" class="dropdown-item">Data Pengajuan Cuti</a>
                                <!-- <a href="{{ url('/datacuti') }}" class="dropdown-item">Data Cuti Karyawan</a> -->
                                <a href="{{ url('/kategoricuti') }}" class="dropdown-item">Kategori Cuti</a>

                            </div>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Pegawai')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/cutip') }}"  ><i class="fa big-icon fa-user-times"></i> Cuti </a>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-table"></i> <span class="mini-dn">Interview</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a href="static-table.html" class="dropdown-item">Static Table</a>
                                <a href="data-table.html" class="dropdown-item">Data Table</a>
                            </div>
                        </li>
                        @endif

                        @if(auth()->user()->level=='Admin')
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-edit"></i> <span class="mini-dn">Kelola Manajemen</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown form-left-menu-std animated flipInX">
                                <a href="{{ url('/jabatan') }}" class="dropdown-item">Jabatan</a>
                                <a href="{{ url('/divisi') }}" class="dropdown-item">Divisi</a>
                                <a href="{{ url('/grade') }}" class="dropdown-item">Grade</a>
                                <a href="{{ url('/corporate') }}" class="dropdown-item">Corporate Grub</a>

                            </div>
                        </li>
                        @endif


                    </ul>
                </div>
            </nav>
        </div>
        <!-- Header top area start-->
<div class="content-inner-all">
    <div class="header-top-area">
        <div class="fixed-header-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                        <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="admin-logo logo-wrap-pro">
                            <a href="#"><img src="material/img/logo/log.png" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">
                        <div class="header-top-menu tabl-d-n">

                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="header-right-info">
                            <ul class="nav navbar-nav mai-top-nav header-right-menu">


                                <li class="nav-item">
                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                        <span class="adminpro-icon adminpro-user-rounded header-riht-inf"></span>
                                        <span class="admin-name">{{auth()->user()->nama}}</span>
                                        <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                                    </a>
                                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">
                                        <li><a href="#"><span class="adminpro-icon adminpro-home-admin author-log-ic"></span>My Account</a>
                                        </li>
                                        <li><a href="#"><span class="adminpro-icon adminpro-user-rounded author-log-ic"></span>My Profile</a>
                                        </li>
                                        <li><a href="#"><span class="adminpro-icon adminpro-money author-log-ic"></span>User Billing</a>
                                        </li>
                                        <li><a href="#"><span class="adminpro-icon adminpro-settings author-log-ic"></span>Settings</a>
                                        </li>
                                        <li><a href="/logout"><span class="adminpro-icon adminpro-locked author-log-ic"></span>Log Out</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header top area end-->
            <!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                          <div class="mobile-menu-area">
                              <div class="container">
                                  <div class="row">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="mobile-menu">
                                              <nav id="dropdown">
                                                  <ul class="mobile-menu-nav">
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Charts" href="#">Home <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul class="collapse dropdown-header-top">
                                                              <li><a href="dashboard.html">Dashboard v.1</a>
                                                              </li>
                                                              <li><a href="dashboard-2.html">Dashboard v.2</a>
                                                              </li>
                                                              <li><a href="analytics.html">Analytics</a>
                                                              </li>
                                                              <li><a href="widgets.html">Widgets</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#demo" href="#">Mailbox <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="demo" class="collapse dropdown-header-top">
                                                              <li><a href="inbox.html">Inbox</a>
                                                              </li>
                                                              <li><a href="view-mail.html">View Mail</a>
                                                              </li>
                                                              <li><a href="compose-mail.html">Compose Mail</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#others" href="#">Miscellaneous <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="others" class="collapse dropdown-header-top">
                                                              <li><a href="profile.html">Profile</a>
                                                              </li>
                                                              <li><a href="contact-client.html">Contact Client</a>
                                                              </li>
                                                              <li><a href="contact-client-v.1.html">Contact Client v.1</a>
                                                              </li>
                                                              <li><a href="project-list.html">Project List</a>
                                                              </li>
                                                              <li><a href="project-details.html">Project Details</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Interface <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="Miscellaneousmob" class="collapse dropdown-header-top">
                                                              <li><a href="google-map.html">Google Map</a>
                                                              </li>
                                                              <li><a href="data-maps.html">Data Maps</a>
                                                              </li>
                                                              <li><a href="pdf-viewer.html">Pdf Viewer</a>
                                                              </li>
                                                              <li><a href="x-editable.html">X-Editable</a>
                                                              </li>
                                                              <li><a href="code-editor.html">Code Editor</a>
                                                              </li>
                                                              <li><a href="tree-view.html">Tree View</a>
                                                              </li>
                                                              <li><a href="preloader.html">Preloader</a>
                                                              </li>
                                                              <li><a href="images-cropper.html">Images Cropper</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Chartsmob" href="#">Charts <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="Chartsmob" class="collapse dropdown-header-top">
                                                              <li><a href="bar-charts.html">Bar Charts</a>
                                                              </li>
                                                              <li><a href="line-charts.html">Line Charts</a>
                                                              </li>
                                                              <li><a href="area-charts.html">Area Charts</a>
                                                              </li>
                                                              <li><a href="rounded-chart.html">Rounded Charts</a>
                                                              </li>
                                                              <li><a href="c3.html">C3 Charts</a>
                                                              </li>
                                                              <li><a href="sparkline.html">Sparkline Charts</a>
                                                              </li>
                                                              <li><a href="peity.html">Peity Charts</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Tablesmob" href="#">Tables <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="Tablesmob" class="collapse dropdown-header-top">
                                                              <li><a href="static-table.html">Static Table</a>
                                                              </li>
                                                              <li><a href="data-table.html">Data Table</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#formsmob" href="#">Forms <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="formsmob" class="collapse dropdown-header-top">
                                                              <li><a href="basic-form-element.html">Basic Form Elements</a>
                                                              </li>
                                                              <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                                              </li>
                                                              <li><a href="password-meter.html">Password Meter</a>
                                                              </li>
                                                              <li><a href="multi-upload.html">Multi Upload</a>
                                                              </li>
                                                              <li><a href="tinymc.html">Text Editor</a>
                                                              </li>
                                                              <li><a href="dual-list-box.html">Dual List Box</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Appviewsmob" href="#">App views <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="Appviewsmob" class="collapse dropdown-header-top">
                                                              <li><a href="basic-form-element.html">Basic Form Elements</a>
                                                              </li>
                                                              <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                                              </li>
                                                              <li><a href="password-meter.html">Password Meter</a>
                                                              </li>
                                                              <li><a href="multi-upload.html">Multi Upload</a>
                                                              </li>
                                                              <li><a href="tinymc.html">Text Editor</a>
                                                              </li>
                                                              <li><a href="dual-list-box.html">Dual List Box</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                      <li>
                                                          <a data-toggle="collapse" data-target="#Pagemob" href="#">Pages <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
                                                          <ul id="Pagemob" class="collapse dropdown-header-top">
                                                              <li><a href="login.html">Login</a>
                                                              </li>
                                                              <li><a href="register.html">Register</a>
                                                              </li>
                                                              <li><a href="captcha.html">Captcha</a>
                                                              </li>
                                                              <li><a href="checkout.html">Checkout</a>
                                                              </li>
                                                              <li><a href="contact.html">Contacts</a>
                                                              </li>
                                                              <li><a href="review.html">Review</a>
                                                              </li>
                                                              <li><a href="order.html">Order</a>
                                                              </li>
                                                              <li><a href="comment.html">Comment</a>
                                                              </li>
                                                          </ul>
                                                      </li>
                                                  </ul>
                                              </nav>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->

            <!-- Mobile Menu start -->

            <!-- Mobile Menu end -->
            <!-- Breadcome End-->
            @yield('content')
            <!-- accordion start-->
                        <!-- accordion End-->
        </div>
    </div>
    <!-- Footer Start-->
    <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>Copyright &#169; 2019-2020 <br> Anak Magang UINSA Prodi <a href="http://ozy-dev.web.id/">Sistem Informasi</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
    <script src="/material/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="/material/js/bootstrap.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="/material/js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="/material/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="/material/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="/material/js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="/material/js/counterup/jquery.counterup.min.js"></script>
    <script src="/material/js/counterup/waypoints.min.js"></script>
    <!-- rounded-counter JS
		============================================ -->
    <script src="/material/js/rounded-counter/jquery.countdown.min.js"></script>
    <script src="material/js/rounded-counter/jquery.knob.js"></script>
    <script src="/material/js/rounded-counter/jquery.appear.js"></script>
    <script src="/material/js/rounded-counter/knob-active.js"></script>
    <!-- peity JS
		============================================ -->
    <script src="/material/js/peity/jquery.peity.min.js"></script>
    <script src="/material/js/peity/peity-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="/material/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="/material/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
		============================================ -->
    <script src="/material/js/flot/jquery.flot.js"></script>
    <script src="/material/js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/material/js/flot/jquery.flot.resize.js"></script>
    <script src="/material/js/flot/widget-flot-chart-active.js"></script>
    <!-- map JS
		============================================ -->
    <script src="/material/js/map/raphael.min.js"></script>
    <script src="/material/js/map/jquery.mapael.js"></script>
    <script src="/material/js/map/france_departments.js"></script>
    <script src="/material/js/map/world_countries.js"></script>
    <script src="/material/js/map/usa_states.js"></script>
    <script src="/material/js/map/map-active.js"></script>
    <!--  editable JS
		============================================ -->
    <script src="/material/js/jquery.mockjax.js"></script>
    <script src="/material/js/mock-active.js"></script>
    <script src="/material/js/select2.js"></script>
    <script src="/material/js/moment.min.js"></script>
    <script src="/material/js/bootstrap-datetimepicker.js"></script>
    <script src="/material/js/bootstrap-editable.js"></script>
    <script src="/material/js/xediable-active.js"></script>
    <!-- modal JS
		============================================ -->
    <script src="/material/js/modal-active.js"></script>
    <!-- icheck JS
		============================================ -->
    <script src="/material/js/icheck/icheck.min.js"></script>
    <script src="/material/js/icheck/icheck-active.js"></script>
    <!-- data table JS
		============================================ -->
    <script src="/material/js/data-table/bootstrap-table.js"></script>
    <script src="/material/js/data-table/tableExport.js"></script>
    <script src="/material/js/data-table/data-table-active.js"></script>
    <script src="/material/js/data-table/bootstrap-table-editable.js"></script>
    <script src="/material/js/data-table/bootstrap-editable.js"></script>
    <script src="/material/js/data-table/bootstrap-table-resizable.js"></script>
    <script src="/material/js/data-table/colResizable-1.5.source.js"></script>
    <script src="/material/js/data-table/bootstrap-table-export.js"></script>
    <!-- main JS
		============================================ -->
    <script src="/material/js/main.js"></script>
</body>

</html>
