<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class FamilysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('familys')->insert([
            [
            'id_personal' => '1',
            'hubungan' => 'adik',
            'nama' => 'Indah Kusuma',
            'jenis_kelamin' => 'perempuan',
            'usia' => '21',
            'pendidikan' => 's1',
            'pekerjaan' => 'swasta',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_personal' => '1',
            'hubungan' => 'kakak',
            'nama' => 'Indah dwi',
            'jenis_kelamin' => 'perempuan',
            'usia' => '21',
            'pendidikan' => 's1',
            'pekerjaan' => 'swasta',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
                ],
        ]);
    }
}
