<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('employees')->insert([
        [
      'nama' => 'Randa',
      'kode_karyawan' => 'randa@gmail.com',
      'password' => Hash::make('1234'),
      'level' => 'Admin',
      'id_jabatan' => '1',
      'id_grade' => '1',
      'id_corporate' => '1',
      'id_devisi' => '1',
      'jenis_kelamin' => 'Laki-laki',
      'homebase' => 'Surabaya',
      'join_date' => '2019-12-26 00:00:00',
      'tempat_lahir' => 'Surabaya',
      'tanggal_lahir' => '1998-03-21 00:00:00',
      'agama' => 'Islam',
      'status' => 'jomblo',
      'alamat_ktp' => 'Surabaya',
      'alamat_domisili' => 'Surabaya',
      'telefon' => '081273338544',
      'email_corporate' => Str::random(10).'@gmail.com',
      'email_pribadi' => Str::random(10).'@gmail.com',
      'no_ktp' => Str::random(15),
      'nama_ibu' => Str::random(10),
      'nama_pasangan' => Str::random(10),
      'pendidikan' => 'Sarjana',
      'emergencycontacts' => Str::random(15),
      'bank_account' => Str::random(15),
      'rekening_number' => Str::random(15),
      'account_name' => Str::random(15),
      'pkwtt' => Str::random(15),
      'nama_atasan' => 'Randa',
      'jatah_cuti' => '10',
      'first_contract' => '2019-12-26 00:00:00',
      'start_addendum1' => '2019-12-26 00:00:00',
      'end_addendum1' => '2019-12-26 00:00:00',
      'start_addendum2' => '2019-12-26 00:00:00',
      'end_addendum2' => '2019-12-26 00:00:00',
      'end_contract' => '2019-12-26 00:00:00',
      'alasan_berhenti' => 'aafafa',
      'foto' => 'wdwdwd',
      'remember_token' => Str::random(15),
      'created_at' => '2019-12-26 00:00:00',
      'updated_at' => '2019-12-26 00:00:00',],
       [
        'nama' => 'miko',
        'kode_karyawan' => 'pegawai@gmail.com',
        'password' => Hash::make('1234'),
        'level' => 'Pegawai',
        'id_jabatan' => '1',
        'id_grade' => '1',
        'id_corporate' => '1',
        'id_devisi' => '1',
        'jenis_kelamin' => 'Laki-laki',
        'homebase' => 'Surabaya',
        'join_date' => '2019-12-26 00:00:00',
        'tempat_lahir' => 'Surabaya',
        'tanggal_lahir' => '1998-03-21 00:00:00',
        'agama' => 'Islam',
        'status' => 'jomblo',
        'alamat_ktp' => 'Surabaya',
        'alamat_domisili' => 'Surabaya',
        'telefon' => '081273338544',
        'email_corporate' => Str::random(10).'@gmail.com',
        'email_pribadi' => Str::random(10).'@gmail.com',
        'no_ktp' => Str::random(15),
        'nama_ibu' => Str::random(10),
        'nama_pasangan' => Str::random(10),
        'pendidikan' => 'Sarjana',
        'emergencycontacts' => Str::random(15),
        'bank_account' => Str::random(15),
        'rekening_number' => Str::random(15),
        'account_name' => Str::random(15),
        'pkwtt' => Str::random(15),
        'nama_atasan' => 'Randa',
        'jatah_cuti' => '10',
        'first_contract' => '2019-09-26 00:00:00',
        'start_addendum1' => '2019-10-26 00:00:00',
        'end_addendum1' => '2019-11-26 00:00:00',
        'start_addendum2' => '2019-12-26 00:00:00',
        'end_addendum2' => '2029-12-27 00:00:00',
        'end_contract' => '2029-12-28 00:00:00',
        'alasan_berhenti' => 'aafafa',
        'foto' => 'wdwdwd',
        'remember_token' => Str::random(15),
        'created_at' => '2019-12-26 00:00:00',
        'updated_at' => '2019-12-26 00:00:00'

       ],
  ]);

    }
}
