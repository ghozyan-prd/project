<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class JobhistorysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_historys')->insert([
            [
            'id_personal' => '1',
            'nama_perusahaan' => 'spanish',
            'posisi' => 'Kepala Gugus',
            'tanggal_mulai' => '2019-12-26 00:00:00',
            'tanggal_selesai' => '2019-12-26 00:00:00',
            'alasan_berhenti' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_personal' => '1',
            'nama_perusahaan' => 'spanish',
            'posisi' => 'Kepala Gugus',
            'tanggal_mulai' => '2019-12-26 00:00:00',
            'tanggal_selesai' => '2019-12-26 00:00:00',
            'alasan_berhenti' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
