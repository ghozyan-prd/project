<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AssettransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asset_transactions')->insert([
            [
                'kode_peminjaman' => 'PA01',
                'id_aset' => '1',
                'pic' => 'Kepala Gugus',
                'tanggal_pinjam' => '2019-12-26 00:00:00',
                'tanggal_kembali' => '2019-12-26 00:00:00',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],
                [
                'kode_aset' => 'PA02',
                'id_aset' => '1',
                'pic' => 'Kepala Gugus',
                'tanggal_pinjam' => '2019-12-26 00:00:00',
                'tanggal_kembali' => '2019-12-26 00:00:00',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],

            ]);
    }
}
