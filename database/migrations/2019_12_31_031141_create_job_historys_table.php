<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_historys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_personal');
            $table->char('nama_perusahaan', 100);
            $table->char('posisi', 100);
            $table->dateTime('tanggal_mulai');
            $table->dateTime('tanggal_selesai');
            $table->char('alasan_berhenti', 100);
            $table->timestamps();

            $table->foreign('id_personal')
                  ->references('id')
                  ->on('personals')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_historys');
    }
}
