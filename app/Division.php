<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends Model
{
    use SoftDeletes;
    protected $fillable = ['kode_devisi', 'nama_devisi'];

    public static function getId(){
      return $getId = DB::table('divisions')->orderBy('id','DESC')->take(1)->get();
}
}