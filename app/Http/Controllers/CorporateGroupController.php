<?php

namespace App\Http\Controllers;

use App\Corporate;
use Illuminate\Http\Request;
use Carbon;

class CorporateGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporate_groups = Corporate::all();
        return view('admin.corporate.index', compact('corporate_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.corporate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Corporate::getId();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_corporate = 'CG'.$idbaru;

        $corp = new Corporate;
        $corp -> kode_corporate_group = $kode_corporate;
        $corp -> nama_corporate_group = $request -> nama_aset;
        $corp  -> save();
        
        return redirect('/corporate')-> with('status', 'Data Corporate Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function show(Corporate $corporate)
    {
    //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function edit(Corporate $corporate)
    {
        return view('admin.corporate.edit', compact('corporate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Corporate $corporate)
    {
        Corporate::where('id',$corporate->id)
        ->update([
          'kode_corporate_group' => $request->kode_corporate_group,
          'nama_corporate_group' => $request->nama_corporate_group
        ]);
        return redirect('/corporate')-> with('edit', 'Data Berhasil di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Corporate $corporate)
    {
        Corporate::destroy($corporate->id);
        return redirect('/corporate')-> with('delete', 'Data Berhasil di Hapus');
    }
    public function trash(){
        $corporate = Corporate::onlyTrashed()->get();
        return view ('admin.corporate.trash', ['corporate' => $corporate]);
       
    }
  
      public function restore($id){
        $corporate = Corporate::onlyTrashed()->where('id', $id);
        $corporate->restore();
        return redirect('/corporate/trash')-> with('restore', 'Data Aset Berhasil di Restore');
      }
  
      public function deleted_permanent($id){
        $corporate_groups = Corporate::onlyTrashed()->where('id', $id);
        $corporate_groups->forceDelete();
  
        return redirect('/corporate/trash')-> with('delete', 'Data Aset Berhasil di Delete Permanent');
      }
  
      public function restore_all(){
        $corporate_groups = Corporate::onlyTrashed();
        $corporate_groups->restore();
  
        return redirect('/corporate/trash')-> with('restore_all', 'Semua Data Berhasil di Restore');
      }
  
      public function deleted_all(){
        $corporate_groups = Corporate::onlyTrashed();
        $corporate_groups->forceDelete();
  
        return redirect('/corporate/trash')-> with('deleted_all', 'Data Aset Berhasil di Delete Semua');
      }

}
