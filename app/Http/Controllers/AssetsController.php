<?php

namespace App\Http\Controllers;

use App\Asset;
use App\AssetTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Asset::all();
        $assets = Asset::all();
        $transaksi = DB::table('asset_transactions')
        ->select('assets.kode_aset','assets.nama_aset','assets.kategori','asset_transactions.tanggal_pinjam','asset_transactions.tanggal_kembali','asset_transactions.kode_peminjaman')
        ->join('assets','asset_transactions.id_aset','assets.id')
        ->get();
        //dd($transaksi);
        return view('admin.assets.index', compact('assets','transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        $id = Asset::getId();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $bulan = date('m-Y');
        $kode_aset = 'QWERTY/'.$idbaru.'/'.$bulan;

        $asset = new Asset;
        $asset -> kode_aset = $kode_aset;
        $asset -> nama_aset = $request -> nama_aset;
        $asset -> tanggal_beli = $request -> tanggal_beli;
        $asset -> kategori = $request -> kategori;
        $asset -> gambar = $request -> gambar;
        $asset -> save();
        // return redirect('/assets');


        // Asset::create([
        //   'kode_aset' => $request->kode_aset,
        //   'nama_aset' => $request->nama_aset,
        //   'deskripsi_aset' => $request->deskripsi_aset,
        //   'tanggal_beli' => $request->tanggal_beli,
        //   'kategori' => $request->kategori,
        //   'sub_kategori' => $request->sub_kategori,
        //   'harga' => $request->harga,
        //   'gambar' => $request->gambar,
        //
        // ]);


        // asset::create($request->all());
        return redirect('/assets')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
      $tes = DB::table('asset_transactions')
      ->select('asset_transactions.kode_peminjaman','assets.nama_aset','asset_transactions.tanggal_pinjam','asset_transactions.tanggal_kembali', 'employees.id', 'employees.nama')
      ->join('assets','asset_transactions.id_aset','assets.id')
      ->join('employees','asset_transactions.id_pegawai','employees.id')
      ->get();

      $users = DB::table('asset_transactions')
                ->select('asset_transactions.id_pegawai','employees.nama')
                ->join('employees','asset_transactions.id','employees.id')
                ->where('asset_transactions.tanggal_kembali', '=', '0')
                ->orderBy('asset_transactions.id', 'desc')
                ->limit(1)
                ->get();

      //dd($users);

        return view('admin.assets.show', compact('asset','tes', 'users'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        return view('admin.assets.edit', compact('asset'));
    }
    public function penyerahan(Asset $asset)
    {
        $ambil = Asset::all();
        return view('admin.assets.penyerahan', compact('asset','ambil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        // return $asset;
        Asset::where('id',$asset->id)
        ->update([
          'kode_aset' => $request->kode_aset,
          'nama_aset' => $request->nama_aset,
          'tanggal_beli' => $request->tanggal_beli,
          'kategori' => $request->kategori,
          'gambar' => $request->gambar,
        ]);
        return redirect('/assets')-> with('edit', 'Data Aset Berhasil di Hapus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
      Asset::destroy($asset->id);
        return redirect('/assets')-> with('delete', 'Data Aset Berhasil di Hapus');
    }

    public function trash(){
      $assets = Asset::onlyTrashed()->get();
      return view ('admin.assets.trash', ['asset' => $assets]);
    }

    public function restore($id){
      $asset = asset::onlyTrashed()->where('id', $id);
      $asset->restore();
      return redirect('/assets/trash')-> with('restore', 'Data Aset Berhasil di Restore');
    }

    public function deleted_permanent($id){
      $assets = Asset::onlyTrashed()->where('id', $id);
      $assets->forceDelete();

      return redirect('/assets/trash')-> with('delete', 'Data Aset Berhasil di Delete Permanent');
    }

    public function restore_all(){
      $assets = Asset::onlyTrashed();
      $assets->restore();

      return redirect('/assets/trash')-> with('restore_all', 'Data Aset Berhasil di Restore all');
    }

    public function deleted_all(){
      $assets = Asset::onlyTrashed();
      $assets->forceDelete();

      return redirect('/assets/trash')-> with('deleted_all', 'Data Aset Berhasil di Delete Semua');
    }

    // public function tes()
    // {
    //     $tes = DB::table('asset_transactions')
    //     ->select('asset_transactions.kode_peminjaman','employees.nama','asset_transactions.tanggal_pinjam','asset_transactions.tanggal_kembali', 'asset_transactions.pic')
    //     ->join('assets','asset_transactions.id_aset','assets.id')
    //     ->get();
    //     dd($tes);
    //     //return view('admin.assets.show', compact('transaksi'));
    // }

    public function transaksi(Asset $asset){

      $transaksi = DB::table('asset_transactions')
      ->select('asset_transactions.id','assets.kode_aset','assets.nama_aset','assets.gambar','assets.kategori','assets.tanggal_beli','asset_transactions.id_pegawai','asset_transactions.tanggal_pinjam','employees.nama')
      ->join('assets','asset_transactions.id_aset','assets.id')
      ->join('employees','asset_transactions.id_pegawai','employees.id')
      ->get();

      // dd($transaksi);
      return view('admin.assets.penyerahan', compact('asset','transaksi'));

    }

    public function input_transaksi(Request $request)
    {
        //return $request;
        $id = AssetTransaction::getId();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $bulan = date('m-Y');
        $kode_peminjaman = 'QWERTY/'.$idbaru.'/'.$bulan;
        // return($request);
        $AssetTransaction = new AssetTransaction;
        $AssetTransaction -> kode_peminjaman = $kode_peminjaman;
        $AssetTransaction -> id_aset = $request -> id_aset;
        $AssetTransaction -> id_pegawai = $request -> id_pegawai;
        $AssetTransaction -> tanggal_pinjam = $request -> tanggal_pinjam;
        $AssetTransaction -> tanggal_kembali = $request -> tanggal_kembali;
        $AssetTransaction -> save();


          //return($request);

        return redirect('/assets')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }
}
