<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;

class jabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::all();
        return view ('admin/jabatan.index',compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Possition  $possition
     * @return \Illuminate\Http\Response
     */
    public function show(Possition $possition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Possition  $possition
     * @return \Illuminate\Http\Response
     */
    public function edit(Possition $possition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Possition  $possition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Possition $possition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Possition  $possition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Possition $possition)
    {
        //
    }
}
