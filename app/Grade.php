<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Grade extends Model
{

    use SoftDeletes;
    protected $fillable = ['kode_grade', 'nama_grade'];

    public static function getId(){
      return $getId = DB::table('grades')->orderBy('id','DESC')->take(1)->get();
}
}
