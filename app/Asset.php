<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use SoftDeletes;
    protected $fillable = ['kode_aset', 'nama_aset', 'tanggal_beli', 'kategori', 'gambar'];

    public static function getId(){
      return $getId = DB::table('assets')->orderBy('id','DESC')->take(1)->get();
}
}
