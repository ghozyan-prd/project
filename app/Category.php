<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table='cuti_categorys';

    use SoftDeletes;
    // protected $fillable = ['kode_corporate_group', 'nama_corporate_group'];

    public static function getId(){
      return $getId = DB::table('cuti_categorys')->orderBy('id','DESC')->take(1)->get();

    }
}
